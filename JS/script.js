document.getElementById('button').addEventListener('click', async () => {
    try {
    const ipResponse = await fetch('https://api.ipify.org/?format=json')
    const ipData = ipResponse.json();
    const ip = ipData.ip;

    const locationResponse = await fetch(`http://ip-api.com/json/`);
                const locationData = await locationResponse.json();
               

                document.getElementById('result').innerText = `
                country: ${locationData.country}
                region: ${locationData.regionName}
                city: ${locationData.city}
                district: ${locationData.district}
                continent: ${locationData.timezone}
               
            `;
        } catch (error) {
            console.error('Error:', error);
        }
    });